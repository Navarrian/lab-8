Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	
The console has it's filter setting to capture message that are level Info and higher. Rather than the logger.log capture all types of messages.
	
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

It comes from the test timer fail and test timer edge test cases.
	
1.  What does Assertions.assertThrows do?

It assert that execution will throw an exception of a certain type and return it.

1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    	 
    	 	The serialVersionUID is the version number of a serializable class. It is
    	 needed for deserialization to confirm loaded classes for an object is 
    	 compatible. Also, without it can lead to an invalid class exception.
    	 
    2.  Why do we need to override constructors?
    
    		In order to associate the data members such as the serialVersionUID
    		with the class.
    	
    3.  Why we did not override other Exception methods?	
    
    		The Exception class only includes constructors and doesn't
    		have any other methods in it.
    		
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

It initializes the logger by reading in the properties.
	
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

README.md uses Markdown thus the .md at the end of the file. It relates to
	bitbucket because their servers use Markdown for text formatting.
	
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	
The assetthrow was expecting a TimerException but received a NullPointerException.
	We need to throw the TimerException outside the try/catch handling braces. 

1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	
The throw within the try/catch block is being processed and the exception is loss.
		So, a NullPointerException is thrown.
		
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException

NullPointerException is a RunTimeException and TimerException is an Exception because it is checked.

1.  Push the updated/fixed source code to your own repository.